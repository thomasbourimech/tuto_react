import logo from "./logo.svg";
import "./App.css";
import ClickCounter from "./components/ClickCounter";
import HoverCounter from "./components/HoverCounter";
function App() {
  return (
    /* test */
    <div className="App">
      <ClickCounter />
      <HoverCounter />
    </div>
  );
}

export default App;
