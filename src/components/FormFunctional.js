import React from "react";
import { useState } from "react";

function FormFunctional() {
  let [topic, setTopic] = useState("angular");
  let [userName, setUserName] = useState("");

  function handleSubmit(event) {
    alert(`${topic} ${userName}`);
    event.preventDefault();
  }
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>username</label>
        <input
          type="text"
          value={userName}
          onChange={(event) => setUserName(event.target.value)}
        ></input>
      </div>
      <div>
        <label>topic</label>
        <select
          value={topic}
          onChange={(event) => setTopic(event.target.value)}
        >
          <option value="react">React</option>
          <option value="angular">Angular</option>
          <option value="vue">Vue</option>
        </select>
      </div>
      <div>
        <button type="submit">submit</button>
      </div>
    </form>
  );
}

export default FormFunctional;
