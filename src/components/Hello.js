import React from "react";

const Hello = () => {
  // return (
  //     <div>
  //         <h1>Hello Boubou</h1>
  //     </div>
  // )
  return React.createElement(
    { id: "hello", className: "dummy" },
    React.createElement("h1", null, "Hello Boubou")
  );
};

export default Hello;
