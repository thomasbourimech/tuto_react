import React from "react";

function Columns() {
  const items = [
    { id: 1, title: "Blah blah blah title 1" },
    { id: 2, title: "Blih blih blih title 2" },
  ];
  return (
    <React.Fragment>
      {items.map((item) => (
        <React.Fragment key={item.id}>
          <h1>Title</h1>
          <p>{item.title}</p>
        </React.Fragment>
      ))}
    </React.Fragment>
  );
}
export default Columns;
